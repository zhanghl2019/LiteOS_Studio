version 1.41.0

欢迎使用我们的新版本。我们在此版本中主要是问题修复和优化，以下是重要更新的介绍：

- STM32 F429、F769编译(makefile)、烧录、调试

- WiFi IoT（Scons）、AI Camera编译（makefile+cygwin）、烧录

- STM32开发板支持jlink、openocd、st-link等方式进行调试

- 预置编译器(arm gcc、risc-v gcc)、构建工具(make.exe,scons+cygwin)、调试工具（openocd、jlink、stutil等）

- 支持编译、烧录、调试、目标板等工程配置

- 工具栏功能实现，编译烧录调试，重启开发板等等，支持keybindings等。
