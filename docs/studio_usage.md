<p align="center">
  <h1 align="center">高效率使用VSCode</h1>
</p>

LiteOS Studio基于VSCode开发，本章介绍一些VSCode使用技巧。

### 常用快捷操作

常用的快捷操作。


#### 全局搜索

Windows: Ctrl + Shift + F 全局搜索

#### 搜索文件名

Windows: Ctrl + P 搜索文件名称

#### 搜索符号

Windows: Ctrl + T  搜索符号、函数名称等

#### 打开集成终端

Windows: Ctrl + \` 集成命令行终端窗口

#### 前进返回

Windows: Alt+LeftArrow， Alt RightArrow 在打开的文件间切换

#### 文件头尾跳转

Windows: Ctrl + Home、Ctrl+End快速跳转文件头部、尾部

#### 删除一行

Windows: Ctrl + x  快速删除一行

#### 复制一行

Windows: Shift + Alt +UpArrow，Shift + Alt +DownArrow 向上、向下复制一行

#### 移动一行

Windows: Alt + UpArrow，Alt + DownArrow向上、向下移动位置

#### 关闭文件

Windows: Ctrl + w  关闭当前打开的文件

#### 打开关闭的文件

Windows: Ctrl + Shift + T 重新打开关闭的文件